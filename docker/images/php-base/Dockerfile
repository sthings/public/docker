ARG PHP_BASE_IMAGE
# hadolint ignore=DL3006
FROM $PHP_BASE_IMAGE

# hadolint ignore=DL3022
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# hadolint ignore=DL3022
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

# move www-data user&group to uid/gid 1000 to easy share permissions with host system
RUN usermod -u 1000 www-data && \
    groupmod -g 1000 www-data

# fix Composer home permissions from root
RUN mkdir -p /var/www/.composer && chown -R www-data:www-data /var/www/.composer

# install dependencies
# hadolint ignore=DL3008
RUN apt-get update -yqq && apt-get -f install -yyq --no-install-recommends \
    git \
    unzip \
    && apt-get clean && \
       rm -rf /var/lib/apt/lists/* \
          /tmp/* \
          /var/tmp/* \
          /var/log/lastlog \
          /var/log/faillog

RUN pecl install mongodb && docker-php-ext-enable mongodb

RUN install-php-extensions \
    amqp \
    bcmath \
    gd \
    imagick \
    intl \
    opcache \
    pcntl \
    pdo_mysql \
    redis \
    zip \
    imap

# set default workdir and user
WORKDIR /var/www/html
USER www-data
